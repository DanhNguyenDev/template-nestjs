drop table if exists "roles_permissions";
drop table if exists "users_roles";
drop table if exists "users";
drop table if exists "roles";
drop table if exists "permissions";

create table "users"(
	id varchar(50) not null primary key,
	username varchar(50) unique default null,
	email varchar(50) unique default null,
	pw varchar(256) not null,
	created_at timestamp default null,
	created_by varchar(50) default null,
	updated_at timestamp default null,
	updated_by varchar(50) default null,
	deleted_at timestamp default null,
	deleted_by varchar(50) default null
);

create table "roles"(
	id varchar(50) not null primary key,
	name varchar(256) unique default null,
	description text default null,
	created_at timestamp default null,
	created_by varchar(50) default null,
	updated_at timestamp default null,
	updated_by varchar(50) default null,
	deleted_at timestamp default null,
	deleted_by varchar(50) default null
);



create table "permissions" (
	id varchar(50) not null primary key,
	name varchar(256) default null,
	description text default null,
	created_at timestamp default null,
	created_by varchar(50) default null,
	updated_at timestamp default null,
	updated_by varchar(50) default null,
	deleted_at timestamp default null,
	deleted_by varchar(50) default null
);

create table "users_roles" (
	id varchar(50) not null primary key,
	user_id varchar(50) not null,
	role_id varchar(50) not null,
	created_at timestamp default null,
	created_by varchar(50) default null,
	updated_at timestamp default null,
	updated_by varchar(50) default null,
	deleted_at timestamp default null,
	deleted_by varchar(50) default null,
	unique(user_id, role_id),
	constraint fk_users_users_roles foreign key(user_id) references users(id),
	constraint fk_roles_users_roles foreign key(role_id) references roles(id)
);


create table "roles_permissions" (
	id varchar(50) not null primary key,
	role_id varchar(50) not null,
	permission_id varchar(50) not null,
	created_at timestamp default null,
	created_by varchar(50) default null,
	updated_at timestamp default null,
	updated_by varchar(50) default null,
	deleted_at timestamp default null,
	deleted_by varchar(50) default null,
	unique(role_id, permission_id),
	constraint fk_roles_roles_permissions foreign key(role_id) references roles(id),
	constraint fk_permissions_roles_permissions foreign key(permission_id) references permissions(id)
);

create table "blood_parameters" (
	id varchar(50) not null primary key,
	user_id varchar(50) not null,
	
)

