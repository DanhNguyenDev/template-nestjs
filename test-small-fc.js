const bscript = require('bcryptjs');

bscript.compareSync();
const ANSI_RESET = '\x1b[0m';
const ANSI_RED = '\x1b[31m';
const ANSI_GREEN = '\x1b[32m';
const ANSI_YELLOW = '\x1b[33m';
const ANSI_BLUE = '\x1b[34m';

// Example usage
console.log(ANSI_RED + 'This text is red.' + ANSI_RESET);
console.log(ANSI_GREEN + 'This text is green.' + ANSI_RESET);
console.log(ANSI_YELLOW + 'This text is yellow.' + ANSI_RESET);
console.log(ANSI_BLUE + 'This text is blue.' + ANSI_RESET);
console.log(chalk.red('Day la mau do'));
