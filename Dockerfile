FROM node:16.3.0-alpine as builder

ENV NODE_ENV build

USER node
# RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# COPY --chown=node:node . /home/node/app

# RUN npm ci \
#     && npm run build

# ---

# FROM node:12-alpine

# ENV NODE_ENV development

# USER node
# WORKDIR /home/node/app

# COPY --from=builder /home/node/app/package*.json /home/node/app/
# COPY --from=builder /home/node/app/dist/ /home/node/app/dist/
# COPY --from=builder /home/node/app/config.yaml /home/node/app/

# RUN npm ci

CMD ["node", "dist/src/main.js"]