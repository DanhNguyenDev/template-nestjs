import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { ApiBody, ApiConsumes, ApiTags } from "@nestjs/swagger";
import { ImageService } from "./image.service";

@ApiTags("Image")
@Controller("image")
export class ImageController {
  constructor(private readonly imageService: ImageService) {}

  @Post("upload")
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    schema: {
      type: "object",
      properties: {
        file: {
          type: "string",
          format: "binary",
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor("file"))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    // const buffer = await new Promise<Buffer>((resolve, reject) => {
    //   const chunks = [];
    //   file.stream.on('data', (chunk) => chunks.push(chunk));
    //   file.stream.on('end', () => resolve(Buffer.concat(chunks)));
    //   file.stream.on('error', (error) => reject(error));
    // });

    const base64 = file.buffer.toString("base64url");
    return { base64 };
  }
}
