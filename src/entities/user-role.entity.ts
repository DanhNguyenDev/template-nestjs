import { Column, Entity, ManyToOne, Unique } from "typeorm";
import { Base } from "./base.entity";
import { User } from "./users.entity";
import { Role } from "src/roles/entities/role.entity";

@Entity({ name: "users_roles" })
@Unique(["user_id", "role_id"])
export class UserRole extends Base<UserRole> {
  @Column({ type: "varchar", length: 50, nullable: false })
  user_id: string;

  @Column({ type: "varchar", length: 50, nullable: false })
  role_id: string;

  @ManyToOne(() => User, (user) => user.userRoles)
  user: User;

  @ManyToOne(() => Role, (role) => role.userRoles)
  role: Role;
}
