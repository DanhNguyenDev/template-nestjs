import { timestamp } from "rxjs";
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from "typeorm";
import { ulid } from "ulid";

@Entity()
export abstract class Base<T> {
  @PrimaryColumn()
  id: string;

  @CreateDateColumn({ type: "timestamp", nullable: true })
  public created_at: Date | null;

  @UpdateDateColumn({ type: "timestamp", nullable: true })
  public updated_at: Date | null;

  @DeleteDateColumn({ type: "timestamp", nullable: true })
  public deleted_at: Date | null;

  @Column("varchar", { nullable: true })
  created_by: string | null;

  @Column("varchar", { nullable: true })
  updated_by: string | null;

  @Column({ type: "varchar", nullable: true })
  deleted_by: string | null;

  @BeforeInsert()
  createIndex() {
    const date = new Date();
    this.id = ulid(date.getTime());
    this.created_at = new Date();
  }

  @BeforeUpdate()
  update() {
    this.updated_at = new Date();
  }
}
