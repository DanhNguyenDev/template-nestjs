import { Column, Entity, OneToMany } from "typeorm";
import { Base } from "./base.entity";
import { UserRole } from "./user-role.entity";

@Entity({ name: "users" })
export class User extends Base<User> {
  @Column({ type: "varchar", nullable: false, length: 50, unique: true })
  username: string;

  @Column({ type: "varchar", nullable: false, length: 50, unique: true })
  email: string;

  @Column({ type: "varchar", nullable: false, length: 256 })
  pw: string;

  @OneToMany(() => UserRole, (userRole) => userRole.user)
  userRoles: UserRole[];
}
