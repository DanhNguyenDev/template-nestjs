import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  Unique,
} from "typeorm";
import { Base } from "./base.entity";
import { Role } from "src/roles/entities/role.entity";
import { Permission } from "src/permissions/entities/permission.entity";

@Entity({ name: "roles_permissions" })
@Unique(["role_id", "permission_id"])
export class RolePermission extends Base<RolePermission> {
  @Column({ type: "varchar", nullable: false, length: 50 })
  role_id: string;

  @Column({ type: "varchar", length: 50, nullable: false })
  permission_id: string;

  @ManyToOne(() => Role, (role) => role.rolePermissions)
  role: Role;

  @ManyToOne(() => Permission, (permission) => Permission)
  permission: Permission;
}
