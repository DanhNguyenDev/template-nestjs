import { Base } from "src/entities/base.entity";
import { RolePermission } from "src/entities/role-permission.entity";
import { BaseEntity, Column, Entity, OneToMany } from "typeorm";
@Entity({ name: "permissions" })
export class Permission extends Base<Permission> {
  @Column({ type: "varchar", nullable: false, length: 256, unique: true })
  name: string;

  @Column({ type: "text" })
  description?: string;

  @OneToMany(
    () => RolePermission,
    (rolePermission) => rolePermission.permission
  )
  rolePermissions: RolePermission[];
}
