import { Injectable } from "@nestjs/common";
import { CreatePermissionDto } from "./dto/create-permission.dto";
import { UpdatePermissionDto } from "./dto/update-permission.dto";
import { Repository } from "typeorm";
import { Permission } from "./entities/permission.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { HttpResult } from "src/common/http/http-result.http";

@Injectable()
export class PermissionsService {
  constructor(
    @InjectRepository(Permission)
    private readonly permissionRepo: Repository<Permission>
  ) {}
  async create(createPermissionDto: CreatePermissionDto) {
    const permission = this.permissionRepo.create(createPermissionDto);
    const permissionDB = await this.permissionRepo.save(permission);
    return new HttpResult({
      status: true,
      message: "SAVE_DATA_SUCCESS",
      data: permissionDB,
    });
  }

  findAll() {
    return `This action returns all permissions`;
  }

  findOne(id: number) {
    return `This action returns a #${id} permission`;
  }

  update(id: number, updatePermissionDto: UpdatePermissionDto) {
    return `This action updates a #${id} permission`;
  }

  remove(id: number) {
    return `This action removes a #${id} permission`;
  }
}
