import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { BaseDto } from "src/dtos/base.dto";

export class CreatePermissionDto extends BaseDto {
  @ApiProperty({
    example: "UPDATE_USER",
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: "UPDATE_USER",
  })
  @IsNotEmpty()
  @IsString()
  description?: string;
}
