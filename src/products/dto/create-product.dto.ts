import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateProductDto {
  @ApiProperty({
    example: "danh",
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: 20,
  })
  @IsNotEmpty()
  @IsNumber()
  age: number;
}
