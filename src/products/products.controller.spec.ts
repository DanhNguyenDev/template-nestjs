import { Test, TestingModule } from "@nestjs/testing";
import { ProductsController } from "./products.controller";
import { ProductsService } from "./products.service";
import * as request from "supertest";
import { INestApplication, ValidationPipe } from "@nestjs/common";
describe("ProductsController", () => {
  let controller: ProductsController;
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [ProductsService],
    }).compile();

    controller = module.get<ProductsController>(ProductsController);
    app = module.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      })
    );
    await app.init();
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
  it("test get api", () => {
    return request(app.getHttpServer())
      .get("/products")
      .expect(200)
      .expect({
        status: true,
        data: [1, 2, 3, 4, 5],
        message: "GET ALL PRODUCT",
      });
  });

  it("test post api success", () => {
    const body = {
      name: "dane",
      age: 10,
    };
    return request(app.getHttpServer())
      .post("/products")
      .send(body)
      .expect(201)
      .expect({
        status: true,
        data: body,
        message: "POST ALL PRODUCT",
      });
  });

  it("test post api fail", () => {
    const body = {
      name: "dane",
      // age: 'aoc',
    };

    try {
      return request(app.getHttpServer())
        .post("/products")
        .send(body)
        .expect(400)
        .expect({
          statusCode: 400,
          message: [
            "age must be a number conforming to the specified constraints",
            "age should not be empty",
          ],
          error: "Bad Request",
        });
    } catch (error) {
      expect(error).toBeDefined();
      console.log(error);
    }
  });
});
