import { Injectable } from "@nestjs/common";
import { CreateProductDto } from "./dto/create-product.dto";
import { UpdateProductDto } from "./dto/update-product.dto";
// import { HttpResult } from 'src/common/http/http-result.http';

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    return {
      status: true,
      data: createProductDto,
      message: "POST ALL PRODUCT",
    };
  }

  findAll() {
    // return new HttpResult({
    //   status: true,
    //   data: [1, 2, 3, 4, 5],
    //   message: 'GET ALL PRODUCT',
    // });
    return {
      status: true,
      data: [1, 2, 3, 4, 5],
      message: "GET ALL PRODUCT",
    };
  }

  findOne(id: number) {
    return `This action returns a #${id} product`;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  remove(id: number) {
    return `This action removes a #${id} product`;
  }
}
