import { Injectable, Logger, NotAcceptableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpResult } from 'src/common/http/http-result.http';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { User } from 'src/entities/users.entity';
import { async } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async validate(username: string, passport: string) {
    const user = await this.usersRepository.findOne({
      where: {
        username,
      },
    });
    if (!user) {
      return null;
    }
    const isValidPassword = await bcrypt.compareSync(passport, user.pw);
    if (!isValidPassword) {
      return new NotAcceptableException('wrong password');
    }

    if (user && isValidPassword) {
      return user;
    }
  }

  async login(user: any) {
    const payload = {
      username: user.username,
      email: user.email,
      id: user.id,
    };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async googleLogin(req) {
    if (!req.user) {
      return {
        user: null,
        message: 'No Information from google',
      };
    }
    return {
      user: req.user,
      message: 'User information from google',
    };
  }
}
