import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthService } from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private configService: ConfigService,
    // private readonly logger: Logger,
    private authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      algorithms: configService.get('jwt.verifyOptions.algorithms'),
      secretOrKey: configService.get('jwt.publicKey'),
      passReqToCallback: true,
    });
    // this.logger.setContext(JwtStrategy.name);
  }
  async validate(req: Request, payload: AuthPayload, done: Function) {
    // Write the data to the file as JSON
    return {
      id: payload.id,
      name: payload.name,
      email: payload.email,
    };
  }
}

export interface AuthPayload {
  id: number | string;
  name: null | string;
  email: string;
}
