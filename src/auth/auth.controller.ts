import { Controller, Get, Post, UseGuards, Request, Req } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { UserLoginDto } from 'src/dtos/user-login.dto';
import { LocalAuthGuard } from './guard/local.guard';
import { AuthenticationGuard } from './guard/jwt-auth.guard';
import { UsersService } from 'src/users/users.service';
import { GoogleGuard } from './guard/google.guard';

@Controller('auth')
@ApiTags('AUTH')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  @ApiBody({ type: UserLoginDto })
  @ApiResponse({
    status: 200,
    description: 'Login-ok',
  })
  async login(@Request() request: any) {
    return this.authService.login(request.user);
  }

  @ApiBearerAuth()
  @UseGuards(AuthenticationGuard)
  @Get('current-user')
  async getUser(@Request() request: any) {
    console.log(request.user.id);
    return this.usersService.getUser(request.user.id);
    // return request.user.id;
  }

  @Get('google')
  @UseGuards(GoogleGuard)
  async googleAuth(@Req() req) {
    console.log('CO_CHAY_QUA_DAY');
    return 'OK';
  }

  @Get('/google/redirect')
  @UseGuards(GoogleGuard)
  googleAuthRedirect(@Req() req) {
    return this.authService.googleLogin(req);
  }
}
