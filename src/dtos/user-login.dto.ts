import { ApiProperty } from '@nestjs/swagger';
import { BaseDto } from './base.dto';
import { IsNotEmpty } from 'class-validator';

export class UserLoginDto extends BaseDto {
  @ApiProperty({
    example: 'danh.admin.1',
  })
  @IsNotEmpty()
  username: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
