import { ApiHideProperty } from '@nestjs/swagger';

export abstract class BaseDto {
  @ApiHideProperty()
  createdBy: string;

  @ApiHideProperty()
  updatedBy: string;
}
