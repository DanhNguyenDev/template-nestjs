import { ApiAcceptedResponse, ApiProperty } from '@nestjs/swagger';
import { BaseDto } from './base.dto';
import { IsNotEmpty, Matches } from 'class-validator';

export class CreateUserDto extends BaseDto {
  @ApiProperty({
    example: 'danh.admin',
    description: 'username to login',
  })
  @IsNotEmpty()
  @Matches(/^[a-zA-Z0-9@\.]{6,50}$/)
  username: string;

  @ApiProperty({
    example: 'danh@gmail.com',
    description: 'email to authenticate',
  })
  @IsNotEmpty()
  email: string;

  @ApiProperty({ example: '123456', description: 'password used to login' })
  @IsNotEmpty()
  pw: string;
}
