import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateUserDto } from './create-user.dto';
import { IsNotEmpty } from 'class-validator';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  //   @ApiProperty({
  //     example: '01GZE0YD6XH0TGBZ2F45676QXW',
  //   })
  //   @IsNotEmpty()
  //   id: string;
}
