import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { DataSource } from "typeorm";

export const dataSource = new DataSource({
  type: "postgres",
  database: "template",
  host: "localhost",
  port: 15432,
  username: "postgres",
  password: "Password123",
  entities: ["dist/**/*.entity{.ts,.js}"],
  migrations: ["./src/migrations/*.ts", "./dist/migrations/*{.ts,.js}"],
  synchronize: true,
});
export class migrationsCreateUsersTable1679986313911
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "users",
        columns: [
          {
            name: "id",
            type: "varchar(50)",
            isPrimary: true,
          },
          {
            name: "username",
            type: "varchar(50)",
          },
          {
            name: "email",
            type: "varchar(191)",
          },
          {
            name: "createdBy",
            type: "varchar(50)",
            default: null,
          },
          {
            name: "createdAt",
            type: "timestamp",
            default: null,
          },
          {
            name: "updatedBy",
            type: "varchar(50)",
            default: null,
          },
          {
            name: "updatedAt",
            type: "timestamp",
            default: null,
          },
        ],
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("users");
  }
}
