import { Body, Controller, Get, Param, Post, Put } from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "src/dtos/create-user.dto";
import { ApiTags } from "@nestjs/swagger";
import { UserLoginDto } from "src/dtos/user-login.dto";
import { UpdateUserDto } from "src/dtos/update-user.dto";

@Controller("users")
@ApiTags("Users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get("/:id")
  async getDetail(@Param("id") id: string) {
    const result = await this.usersService.getUser(id);
    return result;
  }

  @Put("/:id")
  async update(@Param("id") id: string, @Body() input: UpdateUserDto) {
    const result = await this.usersService.update(id, input);
    return result;
  }

  @Post()
  async create(@Body() input: CreateUserDto) {
    return this.usersService.create(input);
  }

  @Post("/login")
  async login(@Body() input: UserLoginDto) {
    const result = await this.usersService.login(input);
    return result;
  }
}
