import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateUserDto } from "src/dtos/create-user.dto";
import { User } from "src/entities/users.entity";
import { Repository } from "typeorm";
const bscript = require("bcryptjs");
import { HttpResult } from "src/common/http/http-result.http";
import * as ulid from "ulid";
import * as moment from "moment";
import { UserLoginDto } from "src/dtos/user-login.dto";
import { UpdateUserDto } from "src/dtos/update-user.dto";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepo: Repository<User>
  ) {}
  async create(input: CreateUserDto) {
    const password = await this.hashPW(input.pw);
    input.pw = password;
    const user = this.usersRepo.create(input);
    const date = new Date();
    user.id = ulid.ulid(date.getTime());
    const userDB = await this.usersRepo.save(user);
    return new HttpResult({
      status: true,
      message: "SAVE_SUCCESS",
      data: userDB,
    });
  }
  private async hashPW(password: string) {
    if (password) {
      const passHashed = await bscript.hash(password, 10);
      return passHashed;
    }
  }

  async login(input: UserLoginDto) {
    const user = await this.usersRepo.findOne({
      where: {
        username: input.username,
      },
    });
    if (!user) {
      return new HttpResult({
        status: false,
        message: "NOT_FOUND",
        data: null,
      });
    }
    const isValid = await bscript.compareSync(input.password, user.pw);
    if (!!isValid) {
      return new HttpResult({
        status: true,
        message: "LOGIN_SUCCESS",
        data: user,
      });
    } else {
      return new HttpResult({
        status: false,
        message: "WRONG_PASSWORD",
        data: null,
      });
    }
  }

  async getUser(id: string) {
    const user = await this.usersRepo.findOne({
      where: {
        id: id,
      },
    });
    if (!user) {
      return new HttpResult({
        status: false,
        message: "NOT_FOUND",
        data: null,
      });
    }
    return new HttpResult({
      status: true,
      message: "OK",
      data: user,
    });
  }

  async update(id: string, input: UpdateUserDto) {
    let user = await this.usersRepo.findOne({ where: { id } });
    if (!user) {
      return new HttpResult({
        status: false,
        message: "NOT_FOUND",
        data: null,
      });
    }
    user = Object.assign(user, input);
    user.updated_at = new Date();
    user.updated_by = id;
    await this.usersRepo.save(user);
    return new HttpResult({
      status: true,
      message: "UPDATE_SUCCESS",
      data: user,
    });
  }

  async getUserByEmail(email: string) {
    const user = await this.usersRepo.findOne({
      where: {
        email,
      },
    });
    return user;
  }
}
