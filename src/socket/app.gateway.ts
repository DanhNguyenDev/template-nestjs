import {
  WebSocketGateway,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
  SubscribeMessage,
  MessageBody,
} from "@nestjs/websockets";
import { Logger, UsePipes, ValidationPipe } from "@nestjs/common";
import { Server, Socket } from "socket.io";
import { HttpResult } from "src/common/http/http-result.http";
import * as moment from "moment";
import { UsersService } from "src/users/users.service";
import { JwtService } from "@nestjs/jwt";
import { User } from "src/entities/users.entity";

@WebSocketGateway({ cors: true })
@UsePipes(new ValidationPipe())
export class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  private registeredTopics: Map<string, string[]> = new Map<string, string[]>();
  constructor(
    private readonly logger: Logger,
    private readonly userService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  @WebSocketServer() public server: Server;
  afterInit(server: Server) {
    this.logger.log(`socket init`);
  }

  async handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id as string}`);
    const user = this.getDataUserFromToken(client);
  }

  async handleConnection(client: Socket) {
    this.logger.log(client.id, "Connected..............................");
    const user: User = await this.getDataUserFromToken(client);

    if (!user) {
      this.logger.log("CAN_NOT_IDENTIFY_USER");
      client.emit("unauthorized", {
        message: "CAN_NOT_IDENTIFY_USER",
      });
      client.disconnect();
      return;
    }
    client.emit("connection", "connect_success");
    this.init(client, { userId: user.id });
  }

  async init(client: Socket, payload: any) {
    this.logger.log("Client init");
    const { userId } = payload;

    // check user info
    console.log(userId);
    const user = await this.userService.getUser(userId);
    console.log(user);

    if (!user) {
      client.emit("unauthorized", {});
      client.disconnect();
      return;
    }
    const registeredTopics = ["room1", "room2"];

    client.join(registeredTopics);
    const date = moment().format("YYYY-MM-DD");
    const data = {
      date,
      topics: registeredTopics,
    };
    client.emit(
      "init",
      new HttpResult({
        data,
      })
    );
  }

  @SubscribeMessage("refund")
  handleEvent(client: Socket, payload: any) {
    client.emit("refund", "refund_success");
  }

  async getDataUserFromToken(client: Socket): Promise<User> {
    const authToken: any = client.handshake?.query?.token;
    try {
      const decoded = this.jwtService.verify(authToken);
      const data = await this.userService.getUser(decoded.userId); // response to function
      return data.data;
    } catch (ex) {
      return null;
    }
  }
}
