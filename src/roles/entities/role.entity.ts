import { Base } from "src/entities/base.entity";
import { RolePermission } from "src/entities/role-permission.entity";
import { UserRole } from "src/entities/user-role.entity";
import { Column, Entity, OneToMany } from "typeorm";

@Entity({ name: "roles" })
export class Role extends Base<Role> {
  @Column({ type: "varchar", nullable: false, length: 256, unique: true })
  name: string;

  @Column({ type: "text" })
  description?: string;

  @OneToMany(() => RolePermission, (rolePermission) => rolePermission.role)
  rolePermissions: RolePermission[];

  @OneToMany(() => UserRole, (userRole) => userRole.role)
  userRoles: UserRole[];
}
