import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { BaseDto } from "src/dtos/base.dto";

export class CreateRoleDto extends BaseDto {
  @ApiProperty({
    example: "USER",
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: "USER",
  })
  @IsNotEmpty()
  @IsString()
  description: string;
}
