import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import configuration from "./config/configuration";
import { ValidationPipe } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const appConfig = configuration() as any;
  app.enableCors();
  app.setGlobalPrefix(appConfig.server.globalPrefix);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      // disableErrorMessages: true,
      // exceptionFactory: (validationErrors: ValidationError[] = []) => {
      //   const errors = iterate(validationErrors)
      //     .map((error) => mapChildrenToValidationErrors(error))
      //     .flatten()
      //     .filter((item) => !!item.constraints)
      //     .map((item) => Object.values(item.constraints))
      //     .flatten()
      //     .toArray();
      //   return new InvalidInputException(
      //     errors.length > 0 ? (errors[0] as string) : '',
      //   );
      // },
    })
  );
  if (appConfig.server.enableOpenAPI === true) {
    const options = new DocumentBuilder()
      .addBearerAuth()
      // .addApiKey(
      //   { type: 'apiKey', name: 'x-client-id', in: 'header' },
      //   'x-client-id',
      // )
      .setTitle("Template system - API")
      .setDescription("API description - by ntdanh1298@gmail.com")
      .setVersion("1.0")
      .addTag("API")
      .build();
    const document = SwaggerModule.createDocument(app, options, {
      ignoreGlobalPrefix: false,
    });
    SwaggerModule.setup("swagger", app, document, {
      swaggerOptions: { persistAuthorization: true },
    });
  }
  const port = appConfig.server.port;
  await app.listen(port);
}
bootstrap();
